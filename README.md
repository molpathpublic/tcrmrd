This package contains scripts and example data for minimal residual disease (MRD) analysis of 
next-generation sequencing (NGS) data generated with the LymphoTrack TRB and TRG clonality assays
(Invivoscribe Inc, San Diego, CA) as described in:

  *Tung JK, Jangam D, Ho CC, Fung E, Khodadoust MS, Kim YH, Zehnder JL, Stehr H, Zhang BM.
  Minimal/measurable residual disease (MRD) monitoring in patients with lymphoid neoplasms
  by high-throughput sequencing of the T-cell receptor.
  (under review)*

The input to the scripts is data processed with the Invivoscribe LymphoTrack Software. The
scripts have been tested with LymphoTrack command-line version v1.4.3_beta and LymphoTrack
desktop version v2.4.3. In order to quantify cell-equivalent MRD level, samples need to be
prepared with the LymphoQuant internal control, available from Invivoscribe.

# Files in this package
* `README.md`                             This readme file
* `LICENSE.txt`                           License terms for using this package (BSD license)
* `scripts/run_qc.py`                     Script for grouping replicate files and generating QC report
* `scripts/mrd_query.py`                  Script for querying clonal sequences and calculating T-cell equivalents
* `example_data/`                         Example data and parameter files as input to above scripts
* `example_output/`                       Example QC and MRD reports as output by above scripts
* `run_example.sh`                        Linux shell script for running the example analysis

# Installation
* Download source code from repository or unzip from archive
* Install Python (>=3.6) and dependencies:
  * pandas 0.25.3
  * XlsxWriter 1.3.7

# Usage example
* Pre-process input data and generate QC report - this will group replicate files into sample-specific folders and generate the QC report (example reports in example_output folder)
  ```
  python scripts/run_qc.py example_data/example_TRB_data -o TRB
  ```
* Same for TRG subassay
  ```
  python scripts/run_qc.py example_data/example_TRG_data -o TRG
  ```
* Prepare the parameter file (see examples in example_data folder)
  * Line 1: subassay name: 'TRB' or 'TRG'
  * Line 2: DNA input mass in ng (for calculating the T-cell equivalents)
  * Line 3 and the subsequent lines: clonosequences for MRD tracking
* Query clonal sequences specified in the parameter file - this will generate the MRD report (examples in example_output folder)
  ```
  python scripts/mrd_query.py example_data/example_TRB_data/test_sample-00A-0000 example_data/TRB_parameter_file.txt -o TRB
  python scripts/mrd_query.py example_data/example_TRG_data/test_sample-00A-0000 example_data/TRG_parameter_file.txt -o TRG
  ```
* Output:
  * TRB_RunQC.xlsx
  * TRB_MRD_report.xlsx
  * TRG_RunQC.xlsx
  * TRG_MRD_report.xlsx



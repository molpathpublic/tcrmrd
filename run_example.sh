# example MRD analysis
python scripts/run_qc.py example_data/example_TRB_data -o TRB
python scripts/run_qc.py example_data/example_TRG_data -o TRG
python scripts/mrd_query.py example_data/example_TRB_data/test_sample-00A-0000 example_data/TRB_parameter_file.txt -o TRB
python scripts/mrd_query.py example_data/example_TRG_data/test_sample-00A-0000 example_data/TRG_parameter_file.txt -o TRG


#!/usr/bin/env python
# Query clonal sequences and calculate T-cell equivalents
#
# Input: 
#   1. path to directory containing sample-specific lymphotrack output
#   2. file containing assay type, input DNA, and clones to be tracked
# Output: 
#   Excel file containing dominant clones, tracked clone frequency, and cell equivalent counts
#
# Version 1.0 (2022-11-06)

import pandas as pd
import re
import xlsxwriter
import sys
import os
import argparse

def mrd_query(directory,mrd_out_name,TCRtarget,DNAinput,targetseqs):

    # lymphoquant sequences (internal control for quantifying cell-equivalents)
    LQTRG1="GGAATCAGCCCAGGGAAGTATGATACTTACGGAAGCACAAGGAAGAACTTGAGAATGATACTGCGAAATCTTATTGAAAATGACTCTGGAGTCTATTACTGTGCCACCTGGGATGGAGCGTCTACGAATTATTATAAGAAACTCTTTGGCAGTG"
    LQTRG2="TGGGTAAGACAAGCAACAAAGTGGAGGCAAGAAAGAATTCTCAAACTCTCACTTCAATCCTTACCATCAAGTCCGTAGAGAAAGAAGACATGGCCGTTTACTACTGTGCTGCGTGGGGTATTATTATAAGAAACTCTTTGGCAGTG"
    LQTRB1="CAAAGCTGCTGTTCCACTACTATGACAAAGATTTTAACAATGAAGCAGACACCCCTGATAACTTCCAATCCAGGAGGCCGAACACTTCTTTCTGCTTTCTTGACATCCGCTCACCAGGCCTGGGGGACGCAGCCATGTACCTGTGTGCCACCAGCAGAGATGGAAAGGGCAGCAATCAGCCCCAGCATTTTGGTGATG"
    LQTRB2="AAGGGCTGAGATTGATCTACTACTCACAGATAGTAAATGACTTTCAGAAAGGAGATATAGCTGAAGGGTACAGCGTCTCTCGGGAGAAGAAGGAATCCTTTCCTCTCACTGTGACATCGGCCCAAAAGAACCCGACAGCTTTCTATCTCTGTGCCTATTAGGGGAGAGTGGGCTCTCCTACGAGCAGTACTTCGGGCCGGGCACCA"

    # positive control sequences
    LPTRB1="GAGTTGCTCATTTACTTTAACAACAACGTTCCGATAGATGATTCAGGGATGCCCGAGGATCGATTCTCAGCTAAGATGCCTAATGCATCATTCTCCACTCTGAAGATCCAGCCCTCAGAACCCAGGGACTCAGCTGTGTACTTCTGTGCCAGCAGTTTCTCGACCTGTTCGGCTAACTATGGCTACACCTTCGGTTCG"
    LPTRB2="GGAGGTGAGAAGGAAGCCCCCGGCCTGGTCCATACCCCACCACCAACTTGCATAATGGGGGGTGATGTCACCCACCCTCCACTCCCCTCAAAGGAGCAGCTGCTCTGGTGGTCTCTCCCAGGCTCTGGGGGCGGACCCATGGGAGGGGCTGTTTTTGTACAAAGCTGTAACATTGTGGGGACAGGGTTTAGAACTCTGGAAACACCATATATTTTGGAGAG"
    LPTRG1="GAAGACTAAGAAACTTGAGGTAAGTAAAAATGCTCACACTTCCACTTCCACTTTGAAAATAAAGTTCTTAGAGAAAGAAGATGAGGTGGTGTACCACTGTGCCTGTCAGATCCTCACAGGGCGGGTTTAAGAAACTCTTTGGCAGTG"
    LPTRG2="GGAATCAGTCGAGAAAAGTATCATACTTATGCAAGCACAGGGAAGAGCCTTAAATTTATACTGGAAAATCTAATTGAACGTGACTCTGGGGTCTATTACTGTGCCACCTGGAAATTTTATTATAAGAAACTCTTTGGCAGTG"

    if TCRtarget=="TRG":
        LQ1_regex = ".*{seq}.*".format(seq=LQTRG1)
        LQ2_regex = ".*{seq}.*".format(seq=LQTRG2)
        LP1_regex = ".*{seq}.*".format(seq=LPTRG1)
        LP2_regex = ".*{seq}.*".format(seq=LPTRG2)
    else:
        LQ1_regex = ".*{seq}.*".format(seq=LQTRB1)
        LQ2_regex = ".*{seq}.*".format(seq=LQTRB2)
        LP1_regex = ".*{seq}.*".format(seq=LPTRB1)
        LP2_regex = ".*{seq}.*".format(seq=LPTRB2)

    #iterate on all replicate files in directory
    targetdf={} #create dictionary with filename:dataframe of target seqs pairs
    filedf={} #create dictionary with filename:dataframe of file metrics (total reads, LQ reads, LP reads)
    dominantclones=pd.DataFrame() #create empty dataframe for identified dominant clones in each replicate file
    numreps=0
    for filename in os.listdir(directory):
        #print('-----', filename)
        #query unique_reads file for target sequences
        if filename.endswith("fastq_unique_reads.tsv"):
            print("analyzing "+filename+"...")
            numreps+=1
            tot_reads=0
            LQ1reads=0
            LQ2reads=0
            LP1reads=0
            LP2reads=0
            samplename=filename
            uniquereads = pd.read_csv(os.path.join(directory, filename), header=None)
            
            #create dataframe of matched read counts for each target sequence, indexed by sequence
            alltargetreads={'reads':[0]*len(targetseqs)}
            targetdf[filename]= pd.DataFrame(alltargetreads)
            targetdf[filename].index=targetseqs
           
            
            #for each row in unique reads file:
            for row in range(len(uniquereads)): 
                
                #extract read counts from header of each read and add to total
                if re.search('^>.*', uniquereads[0][row]):
                    tot_reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row]).group(1)) + tot_reads
                    
                else:
                    tot_reads=tot_reads+0
                
                #for each target seq, extract read counts for reads containing target seq and update targetdf dataframe with respective read count
                for item in targetseqs:
                    targetseq_regex = ".*{seq}.*".format(seq=item)
                    if re.search('{exp}'.format(exp=targetseq_regex), uniquereads[0][row]):
                        targetdf[filename].loc[item]=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + targetdf[filename].loc[item]
                
                #extract read counts for reads containing LymphoQuant1 seq
                if re.search('{exp}'.format(exp=LQ1_regex), uniquereads[0][row]):
                    LQ1reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LQ1reads
                
                #extract read counts for reads containing LymphoQuant2 seq
                if re.search('{exp}'.format(exp=LQ2_regex), uniquereads[0][row]):
                    LQ2reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LQ2reads
                
                #extract read counts for reads containing LowPos1 seq
                if re.search('{exp}'.format(exp=LP1_regex), uniquereads[0][row]):
                    LP1reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LP1reads
                #extract read counts for reads containing LowPos2 seq
                if re.search('{exp}'.format(exp=LP2_regex), uniquereads[0][row]):
                    LP2reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LP2reads
                    
            #add tot_reads and LQreads to file dictionary        
            filedf[filename]={'total reads':tot_reads, 'allLQ1reads': LQ1reads, 'allLQ2reads':LQ2reads, 'allLP1reads':LP1reads, 'allLP2reads':LP2reads}
            
            #for each target sequence, calculate frequencies and cell equivalents and output to excel file with one target sequence per excel worksheet
            for seq in targetseqs:
                 targetdf[filename].at[seq, "freq"] = (targetdf[filename].at[seq, "reads"])/(tot_reads - (LQ1reads+LQ2reads)) #frequency using total reads minus LQ reads
                 #if number of matched reads <5 (noise threshold), then cell equivalents should be equal to zero.
                 if targetdf[filename].at[seq, "reads"] <= 5:
                    targetdf[filename].at[seq, "cells"] = 0
                    targetdf[filename].at[seq, "cells/mil."] = 0
                 else:
                    targetdf[filename].at[seq, "cells"] = ((targetdf[filename].at[seq, "reads"])/(LQ1reads+LQ2reads))*100
                    targetdf[filename].at[seq, "cells/mil."] = (targetdf[filename].at[seq, "cells"])*(6500/DNAinput)
                
       #check for potential new clones from lymphotrack top 10 reads file
        if filename.endswith("merged_top10_searchtop500.tsv"):
            print("analyzing "+filename+"...")
            top10clones = pd.read_csv(os.path.join(directory, filename), sep='\t')
            rank4clonefreq = top10clones["% total reads"][3]
            rank5clonefreq = top10clones["% total reads"][4]
            if TCRtarget=="TRB": #TRB interpretation criteria
                if (top10clones["% total reads"][0:5] >2.5).all(): #oligoclonal if top 5 clones >2.5%
                    print('oligoclonal sequences detected')
                else: #check each clone to see if it meets clonal criteria: freq >2.5% and 5x clone #5
                    for index, row in top10clones.iterrows():
                        rowfreq = row["% total reads"]
                        if (rowfreq > 2.5) & (rowfreq > (5*rank5clonefreq)):
                            print("Clonotype rank #"+str(index+1)+" IS a predominant clonal sequence")
                            dominantclones=dominantclones.append(row)
                        else:
                            print ("Clonotype rank #"+str(index+1)+" is NOT a predominant clonal sequence")
            elif TCRtarget=="TRG": #TRG interpretation criteria       
                if (top10clones["% total reads"][0:3] >2.5).all(): #oligoclonal if top 3 clones >2.5%
                    print('oligoclonal sequences detected')
                else: #check each clone to see if it meets clonal criteria: freq >2.5% and 5x clone #4
                    for index, row in top10clones.iterrows():
                        rowfreq = row["% total reads"]
                        if (rowfreq > 2.5) & (rowfreq > (5*rank4clonefreq)):
                            print("Clonotype rank #"+str(index+1)+" IS a predominant clonal sequence")
                            dominantclones=dominantclones.append(row)
                        else:
                            print ("Clonotype rank #"+str(index+1)+" is NOT a predominant clonal sequence")
     
    if numreps == 0:
	    sys.stderr.write("Error: No *fastq_unique_reads.tsv files found in input directory\n")
	    sys.exit(1)

    #calculate sample totals from file dictionary
    samplereads = 0
    sampleLQ1reads = 0
    sampleLQ2reads = 0

    for i in filedf.values():
        samplereads = samplereads + (i['total reads'])
        sampleLQ1reads = sampleLQ1reads + (i['allLQ1reads'])
        sampleLQ2reads = sampleLQ2reads + (i['allLQ2reads']) 

    #combine dataframes generated for each targetseq
    combinedtargetdf=pd.DataFrame()
    for i in list(targetdf.values()):
        combinedtargetdf = combinedtargetdf.append(i)
    #sum to get totals for each target seq
    totaldf = combinedtargetdf.groupby(combinedtargetdf.index).sum()


    #####writing to output excel file######

    #create output excel files with separate sheet for each target sequence
    outputxls = xlsxwriter.Workbook(mrd_out_name, {'nan_inf_to_errors': True})
    clonesheet = outputxls.add_worksheet("Clones")
    samplesheet = outputxls.add_worksheet("Samples")

    listofseqs = []
    for i in list(range(1, len(targetseqs)+1)):
        listofseqs.append("Seq "+str(i))

    headers = ['Sample', 'Total', 'Matched', 'Freq', 'LQ1 reads', 'LQ2 reads', 'Cells', "Cells/million"]

    for index, seq in enumerate(targetseqs):  #for each worksheet/targetseq  
        worksheet = outputxls.add_worksheet(listofseqs[index])
        outputrow=0
        outputcol=0
        for item in headers: #write headers
            worksheet.write(outputrow, outputcol, item)
            outputcol += 1
        row=1
        for i in range(len (targetdf)): #for each sample write:
            filename=list(targetdf)[i]
            worksheet.write(row, 0, list(targetdf.keys())[i]) #sample name
            worksheet.write(row, 1, filedf[filename]["total reads"]) #total reads
            worksheet.write(row, 2, targetdf[filename].at[seq, "reads"]) #matched reads
            worksheet.write(row, 3, targetdf[filename].at[seq, "freq"]) #matched freq not counting LQ reads
            worksheet.write(row, 4, filedf[filename]["allLQ1reads"]) #LQ1 reads
            worksheet.write(row, 5, filedf[filename]["allLQ2reads"]) #LQ2 reads
            worksheet.write(row, 6, targetdf[filename].at[seq, "cells"]) #cell equivalents
            worksheet.write(row, 7, targetdf[filename].at[seq, "cells/mil."]) #cells/million
            row+=1
        worksheet.write(row, 0, "Sum/Mean")
        worksheet.write(row, 1, samplereads) #sum of reads across all replicates
        worksheet.write(row, 2, totaldf.at[seq, "reads"]) #sum of matched reads across all replicates
        worksheet.write(row, 3, totaldf.at[seq, "freq"]/numreps) #average frequencies across replicates
        worksheet.write(row, 4, sampleLQ1reads) #sum of LQ1 reads
        worksheet.write(row, 5, sampleLQ2reads) #sum of LQ2 reads
        worksheet.write(row, 6, totaldf.at[seq, "cells"]) #sum of cell equivs
        worksheet.write(row, 7, totaldf.at[seq, "cells/mil."]/numreps) #average cells/mil. across replicates
        row+=2
        worksheet.write(row, 0, seq)


    #write identified clones to clones worksheet
    if dominantclones.empty:
        clonesheet.write(0,0, "No predominant clones identified in any replicate")
    else:
        clonesheet.write(0,0, "Predominant clonal sequences identified:")
        columns= ["Rank", "Sequence", "Merge count", "V-gene", "J-gene", "% total reads"]
        rownum = 2
        colnum=0
        for i in columns:
            clonesheet.write(rownum, colnum, i)
            colnum +=1
        rownum +=1
        for index, row in dominantclones.iterrows():
            clonesheet.write(rownum,0, row["Rank"])
            clonesheet.write(rownum,1, row["Sequence"])
            clonesheet.write(rownum,2, row["Merge count"])
            clonesheet.write(rownum,3, row["V-gene"])
            clonesheet.write(rownum,4, row["J-gene"])
            clonesheet.write(rownum,5, row["% total reads"])
            rownum += 1

    #write sample summary worksheet
    samplesheet.write(0,0, "Sample")
    samplesheet.write(0,1, "Total reads")
    samplesheet.write(0,2, "LP1 reads")
    samplesheet.write(0,3, "LP2 reads")
    samplesheet.write(0,4, "LQ1 reads")
    samplesheet.write(0,5, "LQ2 reads")
    col = 6
    for i in list(range(1,len(targetseqs)+1)):
        samplesheet.write(0, col, "Seq "+ str(i) +" Matched")
        col+=1
    col=0
    row=1
    for i in range(len (targetdf)): #for each sample
        filename=list(targetdf)[i]
        samplesheet.write(row, 0, list(targetdf.keys())[i])
        samplesheet.write(row, 1, str(filedf[filename]["total reads"]))
        samplesheet.write(row, 2, str(filedf[filename]["allLP1reads"]))
        samplesheet.write(row, 3, str(filedf[filename]["allLP2reads"]))
        samplesheet.write(row, 4, str(filedf[filename]["allLQ1reads"]))
        samplesheet.write(row, 5, str(filedf[filename]["allLQ2reads"]))
        col = 6
        for seq in targetseqs: #for each targetseq
            samplesheet.write(row, col, targetdf[filename].at[seq, "reads"])
            col +=1 
        row +=1

    outputxls.close()    
    print("wrote", mrd_out_name)
    print("done")
    return dominantclones
    
# ---------------------------------- Main -------------------------------------
if __name__ == '__main__':
	#input parameters
	parser = argparse.ArgumentParser(description='Query clonal sequences and calculate T-cell equivalents')
	parser.add_argument("datadir", help="Path to sample data directory")
	parser.add_argument("paramfile", help="Path to parameter file containing TCR target (TRG or TRB), DNA input (ng), and clonosequences to query, seperated by line breaks")
	parser.add_argument("-o", "--out_prefix", help="prefix for output name (Default: MRD)")
	args = parser.parse_args()
	directory = args.datadir
	paramfile = args.paramfile
	out_prefix = args.out_prefix
	if out_prefix:
	    mrd_out_name = out_prefix + "_MRD_report.xlsx"
	else:
	    mrd_out_name = "MRD_report.xlsx"
	# check input parameters
	if not os.path.exists(directory):
		sys.stderr.write("Error: Input directory {} not found\n".format(directory))
		sys.exit(1)
	if not os.path.exists(paramfile):
		sys.stderr.write("Error: File {} not found\n".format(paramfile))
		sys.exit(1)
	# param file dataframe
	paramfile = pd.read_csv(paramfile, header=None)
	TCRtarget=paramfile.loc[0,0]
	DNAinput=int(float(paramfile.loc[1,0]))
	targetseqs = paramfile.loc[2:].values.flatten().tolist()  
	# Generate MRD output
	dominantclones = mrd_query(directory,mrd_out_name,TCRtarget,DNAinput,targetseqs)

#!/usr/bin/env python
# Group replicate files and generate QC report
#
# Input:
#   path to TRB/TRG data directory
# Output: 
#   RunQC.xlsx file containing QC data
# Note:
#   A subdirectory for each unique sample will be created with corresponding output files for all replicates.
#
# Version 1.0 (2022-11-06)

import pandas as pd
import re
import xlsxwriter
import os
import sys
import shutil
import argparse
import collections

parser = argparse.ArgumentParser(description='Group replicate files and generate QC report')
parser.add_argument("datadir", help="path to directory containing TRG or TRB data")
parser.add_argument("-m", "--move", help="move instead of copy files to sample-specific subdirectories", action="store_true")
parser.add_argument("-o", "--out_prefix", help="prefix for output file name (default: RunQC)")
args = parser.parse_args()
directory = args.datadir
move = args.move
if args.out_prefix:
    outname = str(args.out_prefix)+'_RunQC.xlsx'
else:
    outname = 'RunQC.xlsx'

# get rank1 clones from each sample to query
rank1clonesdict = {}
for filename in os.listdir(directory):
    if filename.endswith("merged_top10_searchtop500.tsv"):
        print("analyzing "+filename+"...")
        top10clones = pd.read_csv(os.path.join(directory,filename), sep='\t')
        filepath=top10clones.columns[0] #determine TCR target from filepath of IVS pipeline output
        if "TRB" in filepath:
            TCRtarget="TRB"
        elif "TRG" in filepath:
            TCRtarget="TRG"
        else:
            sys.stderr.write("Error: Could not determine subassay. Expected 'TRB' or 'TRG' in file path.\n")
            sys.exit(1)
        samplename=filename.split('-')[0]
        rank1cloneseq = top10clones["Sequence"][0]
        if samplename not in rank1clonesdict:
            rank1clonesdict[samplename]= [rank1cloneseq]
        else: rank1clonesdict[samplename].append(rank1cloneseq)
if len(rank1clonesdict) == 0:
    sys.stderr.write("Error: No *merged_top10_searchtop500.tsv file found in input directory\n")
    sys.exit(1)

uniquerank1clones= list(set([y for x in list(rank1clonesdict.values()) for y in x])) #create array of unique sequences only
targetseqs=uniquerank1clones


# lymphoquant sequences (internal control for quantifying cell-equivalents)
LQTRG1="GGAATCAGCCCAGGGAAGTATGATACTTACGGAAGCACAAGGAAGAACTTGAGAATGATACTGCGAAATCTTATTGAAAATGACTCTGGAGTCTATTACTGTGCCACCTGGGATGGAGCGTCTACGAATTATTATAAGAAACTCTTTGGCAGTG"
LQTRG2="TGGGTAAGACAAGCAACAAAGTGGAGGCAAGAAAGAATTCTCAAACTCTCACTTCAATCCTTACCATCAAGTCCGTAGAGAAAGAAGACATGGCCGTTTACTACTGTGCTGCGTGGGGTATTATTATAAGAAACTCTTTGGCAGTG"
LQTRB1="CAAAGCTGCTGTTCCACTACTATGACAAAGATTTTAACAATGAAGCAGACACCCCTGATAACTTCCAATCCAGGAGGCCGAACACTTCTTTCTGCTTTCTTGACATCCGCTCACCAGGCCTGGGGGACGCAGCCATGTACCTGTGTGCCACCAGCAGAGATGGAAAGGGCAGCAATCAGCCCCAGCATTTTGGTGATG"
LQTRB2="AAGGGCTGAGATTGATCTACTACTCACAGATAGTAAATGACTTTCAGAAAGGAGATATAGCTGAAGGGTACAGCGTCTCTCGGGAGAAGAAGGAATCCTTTCCTCTCACTGTGACATCGGCCCAAAAGAACCCGACAGCTTTCTATCTCTGTGCCTATTAGGGGAGAGTGGGCTCTCCTACGAGCAGTACTTCGGGCCGGGCACCA"

# positive control sequences
LPTRB1="GAGTTGCTCATTTACTTTAACAACAACGTTCCGATAGATGATTCAGGGATGCCCGAGGATCGATTCTCAGCTAAGATGCCTAATGCATCATTCTCCACTCTGAAGATCCAGCCCTCAGAACCCAGGGACTCAGCTGTGTACTTCTGTGCCAGCAGTTTCTCGACCTGTTCGGCTAACTATGGCTACACCTTCGGTTCG"
LPTRB2="GGAGGTGAGAAGGAAGCCCCCGGCCTGGTCCATACCCCACCACCAACTTGCATAATGGGGGGTGATGTCACCCACCCTCCACTCCCCTCAAAGGAGCAGCTGCTCTGGTGGTCTCTCCCAGGCTCTGGGGGCGGACCCATGGGAGGGGCTGTTTTTGTACAAAGCTGTAACATTGTGGGGACAGGGTTTAGAACTCTGGAAACACCATATATTTTGGAGAG"
LPTRG1="GAAGACTAAGAAACTTGAGGTAAGTAAAAATGCTCACACTTCCACTTCCACTTTGAAAATAAAGTTCTTAGAGAAAGAAGATGAGGTGGTGTACCACTGTGCCTGTCAGATCCTCACAGGGCGGGTTTAAGAAACTCTTTGGCAGTG"
LPTRG2="GGAATCAGTCGAGAAAAGTATCATACTTATGCAAGCACAGGGAAGAGCCTTAAATTTATACTGGAAAATCTAATTGAACGTGACTCTGGGGTCTATTACTGTGCCACCTGGAAATTTTATTATAAGAAACTCTTTGGCAGTG"

if TCRtarget=="TRG":
    LQ1_regex = ".*{seq}.*".format(seq=LQTRG1)
    LQ2_regex = ".*{seq}.*".format(seq=LQTRG2)
    LP1_regex = ".*{seq}.*".format(seq=LPTRG1)
    LP2_regex = ".*{seq}.*".format(seq=LPTRG2)
else:
    LQ1_regex = ".*{seq}.*".format(seq=LQTRB1)
    LQ2_regex = ".*{seq}.*".format(seq=LQTRB2)
    LP1_regex = ".*{seq}.*".format(seq=LPTRB1)
    LP2_regex = ".*{seq}.*".format(seq=LPTRB2)

#iterate on all replicate files in directory
targetdf={} #create dictionary with filename:dataframe of target seqs pairs
filedf={} #create dictionary with filename:dataframe of file metrics (total reads, LP freq, LQ reads, LP reads)

numreps=0
for filename in os.listdir(directory):
    #query unique_reads file for target sequences
    if filename.endswith("fastq_unique_reads.tsv"):
        print("analyzing "+filename+"...")
        numreps+=1
        tot_reads=0
        LQ1reads=0
        LQ2reads=0
        LP1reads=0
        LP2reads=0
        samplename=filename
        uniquereads = pd.read_csv(os.path.join(directory, filename), header=None)

        #create dataframe of matched read counts for each target sequence, indexed by sequence
        alltargetreads={'reads':[0]*len(targetseqs)}
        targetdf[filename]= pd.DataFrame(alltargetreads)
        targetdf[filename].index=targetseqs

        #for each row in unique reads file:
        for row in range(len(uniquereads)):

            #extract read counts from header of each read and add to total
            if re.search('^>.*', uniquereads[0][row]):
                tot_reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row]).group(1)) + tot_reads

            else:
                tot_reads=tot_reads+0

            #for each target seq, extract read counts for reads containing target seq and update targetdf dataframe with respective read count
            for item in targetseqs:
                targetseq_regex = ".*{seq}.*".format(seq=item)
                if re.search('{exp}'.format(exp=targetseq_regex), uniquereads[0][row]):
                    targetdf[filename].loc[item]=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + targetdf[filename].loc[item]

            #extract read counts for reads containing LymphoQuant1 seq
            if re.search('{exp}'.format(exp=LQ1_regex), uniquereads[0][row]):
                LQ1reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LQ1reads

            #extract read counts for reads containing LymphoQuant2 seq
            if re.search('{exp}'.format(exp=LQ2_regex), uniquereads[0][row]):
                LQ2reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LQ2reads

            #extract read counts for reads containing LowPos1 seq
            if re.search('{exp}'.format(exp=LP1_regex), uniquereads[0][row]):
                LP1reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LP1reads
            #extract read counts for reads containing LowPos2 seq
            if re.search('{exp}'.format(exp=LP2_regex), uniquereads[0][row]):
                LP2reads=int(re.search("[0-9]+_Tcount([0-9]+)_.*", uniquereads[0][row-1]).group(1)) + LP2reads
        #calculate low positive read frequency
        Lowposfreq=(LP1reads+LP2reads)/tot_reads
        #add tot_reads, LPreads, lowpos freq, and LQreads to file dictionary
        filedf[filename]={'total reads':tot_reads, 'lowposfreq':Lowposfreq, 'allLQ1reads': LQ1reads, 'allLQ2reads':LQ2reads, 'allLP1reads':LP1reads, 'allLP2reads':LP2reads}

        #create folder for each specimen and move all files into folder
        if '-_' in filename: #if current file doesn't have SID (contains '-_') i.e. pos/neg control)
            sampleindex=filename.split('-')[0]
            foldername=filename.split('-')[4]
            if not os.path.exists(os.path.join(directory, foldername)): #if folder for foldername doesnt exist, make folder and move files for current index into folder
                os.mkdir(os.path.join(directory, foldername))
                for filename in os.listdir(directory):
                    if filename==foldername:
                        pass
                    elif filename.split('-')[0] == sampleindex:
                        if move:
                            print('moving files for index '+sampleindex)
                            shutil.move(os.path.join(directory, filename), os.path.join(directory,foldername,filename))
                        else:
                            print('copying files for index '+sampleindex)
                            shutil.copy2(os.path.join(directory, filename), os.path.join(directory,foldername,filename))
            else:
                for filename in os.listdir(directory):
                    if filename==foldername:
                        pass
                    elif filename.split('-')[0] == sampleindex:
                        if move:
                            print('moving files for index '+sampleindex)
                            shutil.move(os.path.join(directory, filename), os.path.join(directory, foldername, filename))
                        else:
                            print('copying files for index '+sampleindex)
                            shutil.copy2(os.path.join(directory, filename), os.path.join(directory, foldername, filename))

        else: #for files that have a SID
            sampleindex=filename.split('-')[0]
            foldername=filename.split('-')[4] +'-'+ filename.split('-')[5]+'-'+ (filename.split('-')[6]).split('_')[0]
            if not os.path.exists(os.path.join(directory, foldername)): #if folder for foldername doesnt exist, make folder and move files for current index into folder
                os.mkdir(os.path.join(directory, foldername))
                for filename in os.listdir(directory):
                    if filename==foldername:
                        pass
                    elif filename.split('-')[0] == sampleindex:
                        if move:
                            print('moving files for index '+sampleindex)
                            shutil.move(os.path.join(directory, filename), os.path.join(directory, foldername, filename))
                        else:
                            print('copying files for index '+sampleindex)
                            shutil.copy2(os.path.join(directory, filename), os.path.join(directory, foldername, filename))

            else:
                for filename in os.listdir(directory):
                    if filename==foldername:
                        pass
                    elif filename.split('-')[0] == sampleindex:
                        if move:
                            print('moving files for index '+sampleindex)
                            shutil.move(os.path.join(directory, filename), os.path.join(directory, foldername, filename))
                        else:
                            print('copying files for index '+sampleindex)
                            shutil.copy2(os.path.join(directory, filename), os.path.join(directory, foldername, filename))

#####writing to output excel file######

#create output excel files with separate sheet for each target sequence
outputxls = xlsxwriter.Workbook(outname, {'nan_inf_to_errors': True})
superscript = outputxls.add_format({'font_script': 1})
#clonesheet = outputxls.add_worksheet("Clones")
samplesheet = outputxls.add_worksheet("Samples")
#sort targetdf dictionary so indices are in ascending order
targetdf = collections.OrderedDict(sorted(targetdf.items()))

samplesheet.write(0,0, "Shared top clonosequences: the following table shows the number of matched reads for each unique Rank1 clonosequence among all indices in the run.")
samplesheet.write(1,0, "Sample")
samplesheet.write(1,1, "Total reads")
samplesheet.write(1,2, "LP1 reads")
samplesheet.write(1,3, "LP2 reads")
samplesheet.write(1,4, "Combined LP read freq")
samplesheet.write(1,5, "LQ1 reads")
samplesheet.write(1,6, "LQ2 reads")
col = 7

for seq in targetseqs:
    sampleswithseq = [k for k, v in rank1clonesdict.items() if seq in v] #list of samples containing targetseq
    samplesheet.write(1, col, "Rank1 clonosequence in "+ ', '.join(sampleswithseq))
    col+=1
col=0
#row=1
row=2
for i in range(len (targetdf)): #for each sample
    filename=list(targetdf)[i]
    samplesheet.write(row, 0, list(targetdf.keys())[i])
    samplesheet.write(row, 1, str(filedf[filename]["total reads"]))
    samplesheet.write(row, 2, str(filedf[filename]["allLP1reads"]))
    samplesheet.write(row, 3, str(filedf[filename]["allLP2reads"]))
    samplesheet.write(row, 4, str(filedf[filename]["lowposfreq"]))
    samplesheet.write(row, 5, str(filedf[filename]["allLQ1reads"]))
    samplesheet.write(row, 6, str(filedf[filename]["allLQ2reads"]))
    col = 7
    for seq in targetseqs: #write number of matched reads for each targetseq
        samplesheet.write(row, col, targetdf[filename].at[seq, "reads"])
        col +=1
    row +=1
row +=1

#check if pos control passes QC. Total LP freq must be 10^-4
lte = u'\u2264'
gte = u'\u2265'
samplesheet.write_rich_string(row, 0, "Positive Control QC (0.00009< read freq 10", superscript, "-4", f" <0.001 and total reads {gte}200,000)")
poskey=[value for key, value in filedf.items() if key.split('-')[4].lower() == 'pos' or key.split('-')[4].lower() == 'low_pos']
if not poskey:
    samplesheet.write(row, 1, "no positive control found")
elif ((0.00009 < poskey[0]['lowposfreq'] < 0.001) and (poskey[0]['total reads'] >= 200000)):
	samplesheet.write(row, 1, "PASS")
else:
	samplesheet.write(row, 1, "FAIL")
row +=1

#check if neg control passes QC. LP1 and LP2 reads need to be <=5
negkey=[value for key, value in filedf.items() if key.split('-')[4].lower() == 'neg']
samplesheet.write(row, 0, f"Negative Control QC ({lte}5 reads of low pos and total reads {gte}200,000):")
if not negkey:
    samplesheet.write(row, 1, "no negative control found")
elif ((negkey[0]['allLP1reads'] <= 5) and (negkey[0]['allLP2reads'] <= 5) and (negkey[0]['total reads'] >= 200000)):
	samplesheet.write(row, 1, "PASS")
else:
	samplesheet.write(row, 1, "FAIL")
outputxls.close()
print("wrote", outname)
print("done")

